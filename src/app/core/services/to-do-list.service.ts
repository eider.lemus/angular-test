import { HttpHeaders, HttpClient, HttpParams, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ToDoList } from '../models/to-do-list.model';

@Injectable({
  providedIn: 'root'
})
export class ToDoListService {

  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })

  };

  constructor(private httpClient: HttpClient) {}
  public getTasks(): Observable<any[]> {
    return this.httpClient.get<any[]>(`${environment.api}/task/all`, this.httpOptions);
  }

  public deleteTask(id:string): Observable<any[]> {
    return this.httpClient.put<any[]>(`${environment.api}/task/destroy/${id}`, this.httpOptions);
  }

  public addTask(tarea: ToDoList): Observable<any[]> {
    let body = {
      name: tarea.name,
      description: tarea.description,
      status: tarea.status
    };
    return this.httpClient.post<any[]>(`${environment.api}/task/store`, body, this.httpOptions);
    
  }
  public editTask(tarea: ToDoList,id:string): Observable<any[]> {
    let body = {
      name: tarea.name,
      description: tarea.description,
      status: tarea.status
    };
    return this.httpClient.post<any[]>(`${environment.api}/task/update/${id}`, body, this.httpOptions);
    
  }
}
