import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


import { ToDoList } from '../../core/models/to-do-list.model';
import { ToDoListService } from '../../core/services/to-do-list.service';

@Component({
  selector: 'app-tarea-form',
  templateUrl: './tarea-form.component.html',
  styleUrls: ['./tarea-form.component.css']
})
export class TareaFormComponent implements OnInit {
    public formTarea: FormGroup;
    item: ToDoList;
  
  
    @Input () tarea: ToDoList;
    @Output () ResponseEmitter: EventEmitter<boolean> = new EventEmitter();
    constructor(
      private formBuilder: FormBuilder,
      private toDoListService: ToDoListService,
      private modalService: NgbModal,
    ) {}
    ngOnInit(): void {
      console.log(this.tarea);
      if(!this.tarea){
      this.formTarea = this.formBuilder.group({
          id: ['', ],
          name: ['',Validators.required],
          description: ['',Validators.required],
          status: ['',Validators.required],
        });}else{
          this.formTarea = this.formBuilder.group({
            id: [this.tarea.id,],
            name: [this.tarea.name,Validators.required],
            description: [this.tarea.description,Validators.required],
            status: [this.tarea.status,Validators.required],
          });
        }
    }
  
    addTask(): void{
    if(!this.tarea){
      this.toDoListService.addTask(this.formTarea.value).subscribe(data =>{
        let x = data;
        this.onEmit(true);
        this.modalService.dismissAll();
      });}else{
        this.toDoListService.editTask(this.formTarea.value,this.tarea.id).subscribe(data =>{
          let x = data;
          this.onEmit(true);
          this.modalService.dismissAll();
        });
      }
    }
  
  
  
    onEmit(param:boolean): void {
      return this.ResponseEmitter.emit(param);
    }
  
  

}
