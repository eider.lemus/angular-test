import { Component, OnInit } from '@angular/core'; 
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToDoListService } from '../../../core/services/to-do-list.service';
import { ToDoList } from '../../../core/models/to-do-list.model';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {

  items: any;
  tarea: ToDoList;

  constructor(
    private toDoListService: ToDoListService,
    private modalService: NgbModal,
  ) {
    
   }

  ngOnInit(): void {
    this.getTask();
  }


  destroy(id:string):void {
    this.toDoListService.deleteTask(id).subscribe(data=>{

      this.getTask();
    })
  }
  public configTarea(content:any): void {
    this.tarea = undefined;
    this.modalService.open(content, { size: 'lg', scrollable: true });
  }
  public editTarea(content:any, tarea:ToDoList): void {
   this.tarea = tarea;
    this.modalService.open(content, { size: 'lg', scrollable: true });
  }

  getTask(){
    this.items = [];
    this.toDoListService.getTasks().subscribe(data => {
      this.items = data;
    });
  }

  capturar(){
    
    this.getTask();
  }
}
