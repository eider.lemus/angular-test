#!/bin/bash

# Crear carpetas personalizadas para la aplicación Angular
mkdir -p src/app/core/constants
mkdir -p src/app/core/guards
mkdir -p src/app/core/interceptors
mkdir -p src/app/core/models
mkdir -p src/app/core/services
mkdir -p src/app/features/feature-1
mkdir -p src/app/features/feature-2
mkdir -p src/app/features/feature-3
mkdir -p src/app/modules/dashboard
mkdir -p src/app/shared/components
mkdir -p src/app/shared/directives
mkdir -p src/app/shared/pipes

# Crear archivo core.module.ts
touch src/app/core/core.module.ts

# Crear archivo dashboard.module.ts
touch src/app/modules/dashboard/dashboard.module.ts

# Crear archivo features.module.ts
touch src/app/features/features.module.ts

# Crear archivo shared.module.ts
touch src/app/shared/shared.module.ts